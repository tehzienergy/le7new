$(".form-start__input").focus();

window.sr = ScrollReveal();

sr.reveal('.front__tablet', { 
  origin: 'bottom',
  duration: 800,
  easing: 'ease-out',
  distance: '3rem',
  scale: 1,
  mobile: false,
  reset: true,
  opacity: 0,
  viewFactor: .4
});

sr.reveal('.front__header', { 
  origin: 'bottom',
  duration: 800,
  easing: 'ease-out',
  distance: '3rem',
  scale: 1,
  mobile: false,
  delay: 100,
  reset: true,
  opacity: 0,
  viewFactor: .2
});

sr.reveal('.front__subheader', { 
  origin: 'bottom',
  duration: 800,
  easing: 'ease-out',
  distance: '3rem',
  scale: 1,
  mobile: false,
  reset: true,
  delay: 200,
  opacity: 0,
  viewFactor: .2
});

sr.reveal('.front__dscr', { 
  origin: 'bottom',
  duration: 800,
  easing: 'ease-out',
  distance: '3rem',
  scale: 1,
  mobile: false,
  reset: true,
  delay: 200,
  opacity: 0,
  viewFactor: .2
});

sr.reveal('.form-start', { 
  origin: 'bottom',
  duration: 800,
  easing: 'ease-out',
  distance: '3rem',
  scale: 1,
  mobile: false,
  reset: true,
  delay: 200,
  opacity: 0,
  viewFactor: .2
});

sr.reveal('.advantages-item__icon', { 
  origin: 'bottom',
  duration: 1000,
  easing: 'ease-out',
  distance: '3rem',
  scale: 1,
  mobile: false,
  reset: true,
  opacity: 0,
  delay: 300,
  viewFactor: 1
});

sr.reveal('.offer__dscr', { 
  origin: 'bottom',
  duration: 1000,
  easing: 'ease-out',
  distance: '3rem',
  scale: 1,
  mobile: false,
  reset: true,
  opacity: 0,
  viewFactor: .1
});

